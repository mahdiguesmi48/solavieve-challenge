import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgRatingBarModule } from 'ng-rating-bar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgRatingBarModule,
    NgbModule    



  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
